package com.murugan.technicaltest.utils.common

interface LoadMoreListener {

    fun onLoadMore()
}