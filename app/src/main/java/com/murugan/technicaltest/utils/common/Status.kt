package com.murugan.technicaltest.utils.common

enum class Status {
    SUCCESS,
    ERROR,
    LOADING,
    UNKNOWN
}