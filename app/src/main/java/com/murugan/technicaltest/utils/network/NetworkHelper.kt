package com.murugan.technicaltest.utils.network

import com.murugan.technicaltest.utils.network.NetworkError

interface NetworkHelper {

    fun isNetworkConnected(): Boolean

    fun castToNetworkError(throwable: Throwable): NetworkError

}