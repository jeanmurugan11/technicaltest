package com.murugan.technicaltest.di.component

import com.murugan.technicaltest.di.ViewModelScope
import com.murugan.technicaltest.di.module.ViewHolderModule
import com.murugan.technicaltest.ui.dummies.DummyItemViewHolder
import com.murugan.technicaltest.ui.home.posts.PostItemViewHolder
import dagger.Component

@ViewModelScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [ViewHolderModule::class]
)
interface ViewHolderComponent {

    fun inject(viewHolder: DummyItemViewHolder)

    fun inject(viewHolder: PostItemViewHolder)
}