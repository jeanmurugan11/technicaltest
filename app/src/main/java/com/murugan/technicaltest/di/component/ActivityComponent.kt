package com.murugan.technicaltest.di.component

import com.murugan.technicaltest.di.ActivityScope
import com.murugan.technicaltest.di.module.ActivityModule
import com.murugan.technicaltest.ui.dummy.DummyActivity
import com.murugan.technicaltest.ui.login.LoginActivity
import com.murugan.technicaltest.ui.main.MainActivity
import com.murugan.technicaltest.ui.splash.SplashActivity
import dagger.Component

@ActivityScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [ActivityModule::class]
)
interface ActivityComponent {

    fun inject(activity: SplashActivity)

    fun inject(activity: LoginActivity)

    fun inject(activity: DummyActivity)

    fun inject(activity: MainActivity)
}