package com.murugan.technicaltest.di.component

import com.murugan.technicaltest.di.FragmentScope
import com.murugan.technicaltest.di.module.FragmentModule
import com.murugan.technicaltest.ui.dummies.DummiesFragment
import com.murugan.technicaltest.ui.home.HomeFragment
import com.mindorks.bootcamp.instagram.ui.photo.PhotoFragment
import com.murugan.technicaltest.ui.profile.ProfileFragment
import dagger.Component

@FragmentScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [FragmentModule::class]
)
interface FragmentComponent {

    fun inject(fragment: DummiesFragment)

    fun inject(fragment: ProfileFragment)

    fun inject(fragment: PhotoFragment)

    fun inject(fragment: HomeFragment)
}