package com.murugan.technicaltest.di.module

import androidx.lifecycle.LifecycleRegistry
import com.murugan.technicaltest.di.ViewModelScope
import com.murugan.technicaltest.ui.base.BaseItemViewHolder
import dagger.Module
import dagger.Provides

@Module
class ViewHolderModule(private val viewHolder: BaseItemViewHolder<*, *>) {

    @Provides
    @ViewModelScope
    fun provideLifecycleRegistry(): LifecycleRegistry = LifecycleRegistry(viewHolder)
}