package com.murugan.technicaltest

import android.app.Application
import com.murugan.technicaltest.di.component.ApplicationComponent
import com.murugan.technicaltest.di.component.DaggerApplicationComponent
import com.murugan.technicaltest.di.module.ApplicationModule


class InstagramApplication : Application() {

    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        injectDependencies()
    }

    private fun injectDependencies() {
        applicationComponent = DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()
        applicationComponent.inject(this)
    }

    // Needed to replace the component with a test specific one
    fun setComponent(applicationComponent: ApplicationComponent) {
        this.applicationComponent = applicationComponent
    }
}