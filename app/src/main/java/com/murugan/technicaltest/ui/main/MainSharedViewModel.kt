package com.murugan.technicaltest.ui.main

import androidx.lifecycle.MutableLiveData
import com.murugan.technicaltest.ui.base.BaseViewModel
import com.murugan.technicaltest.data.model.Post
import com.murugan.technicaltest.utils.common.Event
import com.murugan.technicaltest.utils.network.NetworkHelper
import com.murugan.technicaltest.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

class MainSharedViewModel(
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable,
    networkHelper: NetworkHelper
) : BaseViewModel(schedulerProvider, compositeDisposable, networkHelper) {

    override fun onCreate() {}

    val homeRedirection = MutableLiveData<Event<Boolean>>()

    val newPost: MutableLiveData<Event<Post>> = MutableLiveData()

    fun onHomeRedirect() {
        homeRedirection.postValue(Event(true))
    }
}