package com.murugan.technicaltest.ui.dummy

import com.murugan.technicaltest.ui.base.BaseViewModel
import com.murugan.technicaltest.data.repository.DummyRepository
import com.murugan.technicaltest.utils.network.NetworkHelper
import com.murugan.technicaltest.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable


class DummyViewModel(
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable,
    networkHelper: NetworkHelper,
    private val dummyRepository: DummyRepository
) : BaseViewModel(schedulerProvider, compositeDisposable, networkHelper) {

    override fun onCreate() {
        // do something
    }
}