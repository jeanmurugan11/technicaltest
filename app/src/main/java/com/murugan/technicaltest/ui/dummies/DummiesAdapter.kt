package com.murugan.technicaltest.ui.dummies

import android.view.ViewGroup
import androidx.lifecycle.Lifecycle

import com.murugan.technicaltest.data.model.Dummy
import com.murugan.technicaltest.ui.base.BaseAdapter

class DummiesAdapter(
    parentLifecycle: Lifecycle,
    private val dummies: ArrayList<Dummy>
) : BaseAdapter<Dummy, DummyItemViewHolder>(parentLifecycle, dummies) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = DummyItemViewHolder(parent)
}