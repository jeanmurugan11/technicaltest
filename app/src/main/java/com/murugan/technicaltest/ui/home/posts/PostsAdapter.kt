package com.murugan.technicaltest.ui.home.posts

import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import com.murugan.technicaltest.data.model.Post
import com.murugan.technicaltest.ui.base.BaseAdapter

class PostsAdapter(
    parentLifecycle: Lifecycle,
    posts: ArrayList<Post>
) : BaseAdapter<Post, PostItemViewHolder>(parentLifecycle, posts) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PostItemViewHolder(parent)
}