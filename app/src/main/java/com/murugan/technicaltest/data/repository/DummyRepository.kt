package com.murugan.technicaltest.data.repository


import com.murugan.technicaltest.data.local.db.DatabaseService
import com.murugan.technicaltest.data.model.Dummy
import com.murugan.technicaltest.data.remote.NetworkService
import com.murugan.technicaltest.data.remote.request.DummyRequest
import io.reactivex.Single
import javax.inject.Inject

class DummyRepository @Inject constructor(
    private val networkService: NetworkService,
    private val databaseService: DatabaseService
) {

    fun fetchDummy(id: String): Single<List<Dummy>> =
        networkService.doDummyCall(DummyRequest(id)).map { it.data }

}